function check_for_animable_elements(){

    //Chequeo si hay elementos con la clase .animable en la página
    if($('.animable').length > 0){
        return true;
    }
}

function hide_element(el){
    el.addClass('animable-hide');
}

function check_for_pre_hide_elements(){
    $('.animable').each(function(){
        if($(this).attr('animation-start-hide')){
            if($(this).attr('animation-start-hide') === 'true'){
                hide_element($(this));
            }
        }else{

            //Por defecto escondemos los elementos que no tengan definido el atributo 'animation-start-hide'
            hide_element($(this));
        }
    });
}

function animate_element_start(el){

    if(el.attr('animation-name')){
        el.removeClass('animable-hide');
        el.addClass('animated ' + el.attr('animation-name'));
    }

}

function constructAnimableArray(){

    var animableObj = [];

    $('.animable').each(function(index){

        var self = this;

        animableObj[index] = [];

        animableObj[index]['el'] = $(self);
        animableObj[index]['offset'] = $(self).offset().top;

        if($(self).attr('animation-delay')) {
            animableObj[index]['delay'] = $(self).attr('animation-delay')
        }

        if($(self).attr('animation-order')) {
            animableObj[index]['order'] = $(self).attr('animation-order');
        }else{
            animableObj[index]['order'] = 0;
        }

    });

    //Ordeno el array por 'offset' y lo subordeno por 'order'

    animableObj.sort(function(a, b) {
        var offsetA = parseFloat(a.offset);
        var offsetB = parseFloat(b.offset);

        // Sort first on offset
        if(offsetA > offsetB) {
            return 1;
        } else if (offsetA < offsetB) {
            return -1;
        } else {
            // If the offsets are the same,
            // do a nested sort on total.
            var orderA = a.order;
            var orderB = b.order;

            if(orderA > orderB) {
                return 1;
            } else if (orderA < orderB) {
                return -1;
            } else {
                return 0;
            }
        }
    });

    //Guardo el array de objetos animables en una variable de ventana
    window.animableObj = animableObj;

    /*console.log('array construido')*/
}

var lastDocumentHeight = window.documentHeightAtLoad;

function correctAnimableArray(){
    setInterval(function(){
        if(lastDocumentHeight !== $(document).height()){
            /*console.log('documentHeight corregido:' + $(document).height());*/
            constructAnimableArray();
            animable_scrollAction();
            lastDocumentHeight = $(document).height();
        }
    }, 100);
}

function animable_pageLoad(){
    if(check_for_animable_elements()){

        window.documentHeightAtLoad = $(document).height();

        /*console.log('documentHeightAtLoad: ' + window.documentHeightAtLoad);*/

        constructAnimableArray();
        correctAnimableArray();
    }
}

var scroll_point = 0;

var animable_delay = 0;

if($(window).height() < 640){
    var scroll_compensation = 0;
}else{
    var scroll_compensation = $(window).height()*0.15;
}


function animable_scrollAction(){

    scroll_point = ($(window).scrollTop() + $(window).height()) - scroll_compensation;

    /*console.log('scroll_point: ' + scroll_point);*/

    $(window.animableObj).each(function(index){
        if(this.offset <= scroll_point){

            if(this.delay){
                animable_delay = this.delay;
            }else{
                animable_delay = 0;
            }
            var elementToAnimate = this.el;

            setTimeout(function () {
                animate_element_start(elementToAnimate)
            }, index*animable_delay);
        }
    });
}

$(document).ready(function(){
    check_for_pre_hide_elements();
});

$(window).on({
    'load': function(e) {
        animable_pageLoad();
        animable_scrollAction();
    },
    'touchmove': function(e) {
        animable_scrollAction();
    },
    'scroll': function(e){
        animable_scrollAction();
    },
    'resize': function(e){
        correctAnimableArray();
    }
});
